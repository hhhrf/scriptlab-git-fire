# git-fire

... helps you to get faster out of the building by pushing all changes in your repositor(y/ies) to remote.

The following steps are executed in case there are changes within the repository:

  1. create new branch w/ current timestamp (e.g. fire_20180413-0005)
  2. add all untracked files to this branch
  3. commit and push to remote

## Usage

`git-fire secure` Adds the current repository to the watchlist
`git-fire unsecure` Removes the current repository from the watchlist

`git-fire` Pushes all changes to remote
