#!/bin/bash

MODE=$1
SCRIPT_NAME=git-fire
SCRIPT_ALIAS=""
DEPENDENCIES=()

install() {

  printf "\n # Installing: $SCRIPT_NAME\n\n"

  # Check dependencies
  for i in ${DEPENDENCIES[@]}
  do
    printf " * Checking dependencies : ${i}"
    type ${i} >/dev/null 2>&1 || { printf " NOT FOUND!\n" ; echo "Aborted."; exit 1; }
    printf "\r\033[K"
  done

  printf " * Checking dependencies : OK\n"

  printf " * Creating symlink .... : "
  eval "ln -s $SCRIPT_DIR/$SCRIPT_NAME.sh /usr/local/bin/$SCRIPT_NAME"
  if [ ! -z "$SCRIPT_ALIAS" ]
  then
    type $SCRIPT_ALIAS >/dev/null 2>&1 || { eval "ln -s $SCRIPT_DIR/$SCRIPT_NAME.sh /usr/local/bin/$SCRIPT_ALIAS"; } 
  fi
  printf "Done\n"

  printf " * Creating files ...... :"
  eval "touch $SCRIPT_DIR/repos.dat"
  printf "Done\n\n"

  printf "\n * [$SCRIPT_NAME] Installed!\n"
}

uninstall() {

  [[ ! -d $SCRIPT_DIR/../$SCRIPT_NAME ]] && { echo " $SCRIPT_NAME does not exist. Aborting."; exit 1; } 
  type $SCRIPT_NAME >/dev/null 2>&1 || { echo " $SCRIPT_NAME is not installed. Aborting."; exit 1 ; }

  printf " * Uninstalling $SCRIPT_NAME: "
  eval "rm /usr/local/bin/$SCRIPT_NAME"
  if [ ! -z "$SCRIPT_ALIAS" ]
  then
    eval "rm /usr/local/bin/$SCRIPT_ALIAS"
  fi
  printf "Done\n"

}


#
# installer.sh :: main
#

SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ]; do 
  DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
  SOURCE="$(readlink "$SOURCE")"
  [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE" 
done
SCRIPT_DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"

# Check root
[[ ! $EUID > 0 ]] || { echo " > Root privilege required!"; exit 1; }

case $MODE in

  install)
    install
    ;;

  uninstall)
    uninstall
    ;;

esac
